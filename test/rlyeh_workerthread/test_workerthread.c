/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_workerthread.h"
#include "../../include_priv/rlyeh_worker.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static int32_t myint = 0;

static int32_t task_counter = 0;
static pthread_mutex_t task_counter_mutex = PTHREAD_MUTEX_INITIALIZER;

static tr181_fault_type_t task_func_set_myint(UNUSED amxd_object_t* obj, amxc_var_t* args) {
    myint = GET_INT32(args, NULL);

    rlyeh_worker_signal_task_complete();
    return tr181_fault_ok;
}

static tr181_fault_type_t task_func_task_counter(UNUSED amxd_object_t* obj, amxc_var_t* args) {
    uint32_t s = GET_INT32(args, NULL);
    sleep(s);
    pthread_mutex_lock(&task_counter_mutex);
    task_counter += 1;
    pthread_mutex_unlock(&task_counter_mutex);

    rlyeh_worker_signal_task_complete();
    return tr181_fault_ok;
}

#define SLEEP "sleep"
#define STRING "string"
static tr181_fault_type_t task_func_task_counter_string(UNUSED amxd_object_t* obj, amxc_var_t* args) {
    uint32_t s = GET_INT32(args, SLEEP);
    char* str1 = strdup(GET_CHAR(args, STRING));
    const char* str2 = NULL;
    sleep(s);
    str2 = GET_CHAR(args, STRING);
    assert_string_equal(str1, str2);
    free(str1);
    pthread_mutex_lock(&task_counter_mutex);
    task_counter += 1;
    pthread_mutex_unlock(&task_counter_mutex);

    rlyeh_worker_signal_task_complete();
    return tr181_fault_ok;
}

/*****************************************************************************/

void rlyeh_workerthread_init(UNUSED void** state) {
    assert_int_equal(rlyeh_init_worker(), 0);
    rlyeh_clean_worker();
}

void rlyeh_workerthread_add(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_int32_t(&args, 22);
    assert_int_equal(rlyeh_init_worker(), 0);
    assert_int_equal(rlyeh_worker_add_task(NULL, &args, task_func_set_myint, true, false), 0);
    sleep(1);
    assert_int_equal(myint, 22);
    rlyeh_clean_worker();
}

void rlyeh_workerthread_add_multi_1(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_init(&args);
    task_counter = 0;
    int total_tasks = 10;
    assert_int_equal(rlyeh_init_worker(), 0);
    for(int i = 0; i < total_tasks; i++) {
        int s = 0;
        printf("Add task with sleep %d\n", s);
        amxc_var_set_int32_t(&args, s);
        assert_int_equal(rlyeh_worker_add_task(NULL, &args, task_func_task_counter, true, false), 0);
    }

    bool completed = false;
    while(!completed) {
        pthread_mutex_lock(&task_counter_mutex);
        completed = (task_counter == total_tasks);
        printf("completed %d Task_counter %d total tasks %d\n", completed, task_counter, total_tasks);
        pthread_mutex_unlock(&task_counter_mutex);
        sleep(1);
    }

    rlyeh_clean_worker();
}

void rlyeh_workerthread_add_multi_2(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_init(&args);
    task_counter = 0;
    int total_tasks = 10;
    assert_int_equal(rlyeh_init_worker(), 0);
    for(int i = 0; i < total_tasks; i++) {
        int s = i % 2;
        printf("Add task with sleep %d\n", s);
        amxc_var_set_int32_t(&args, s);
        assert_int_equal(rlyeh_worker_add_task(NULL, &args, task_func_task_counter, true, false), 0);
    }

    bool completed = false;
    while(!completed) {
        pthread_mutex_lock(&task_counter_mutex);
        completed = (task_counter == total_tasks);
        printf("completed %d Task_counter %d total tasks %d\n", completed, task_counter, total_tasks);
        pthread_mutex_unlock(&task_counter_mutex);
        sleep(1);
    }

    rlyeh_clean_worker();
}

void rlyeh_workerthread_add_multi_3(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_init(&args);
    task_counter = 0;
    int total_tasks = 10;
    assert_int_equal(rlyeh_init_worker(), 0);
    for(int i = 0; i < total_tasks; i++) {
        int s = (i % 3 == 0 ? 1 : 0);
        printf("Add task with sleep %d\n", s);
        amxc_var_set_int32_t(&args, s);
        assert_int_equal(rlyeh_worker_add_task(NULL, &args, task_func_task_counter, true, false), 0);
    }

    bool completed = false;
    while(!completed) {
        pthread_mutex_lock(&task_counter_mutex);
        completed = (task_counter == total_tasks);
        printf("completed %d Task_counter %d total tasks %d\n", completed, task_counter, total_tasks);
        pthread_mutex_unlock(&task_counter_mutex);
        sleep(1);
    }

    rlyeh_clean_worker();
}

void rlyeh_workerthread_add_multi_strings(UNUSED void** state) {
    task_counter = 0;
    int total_tasks = 10;
    assert_int_equal(rlyeh_init_worker(), 0);
    for(int i = 0; i < total_tasks; i++) {
        amxc_var_t args;
        char* str = NULL;
        amxc_var_init(&args);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        int s = (i % 3 == 0 ? 1 : 0);
        assert_int_not_equal(asprintf(&str, "This is task number %d", i), -1);
        printf("Add task with sleep %d\n", s);
        amxc_var_add_new_key_int32_t(&args, SLEEP, s);
        amxc_var_add_new_key_cstring_t(&args, STRING, str);
        assert_int_equal(rlyeh_worker_add_task(NULL, &args, task_func_task_counter_string, true, false), 0);
        free(str);
    }

    bool completed = false;
    while(!completed) {
        pthread_mutex_lock(&task_counter_mutex);
        completed = (task_counter == total_tasks);
        printf("completed %d Task_counter %d total tasks %d\n", completed, task_counter, total_tasks);
        pthread_mutex_unlock(&task_counter_mutex);
        sleep(1);
    }

    rlyeh_clean_worker();
}
