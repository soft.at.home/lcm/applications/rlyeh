/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <rlyeh/rlyeh_utils.h>

#include "test_common.h"
#include "../../include_priv/rlyeh_common.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

void rlyeh_test_get_free_disk_space(UNUSED void** state) {
    int64_t diskspace;

    // valid path
    diskspace = get_free_disk_space("/tmp/");
    assert_true(diskspace > 0);

    // invalid path
    diskspace = get_free_disk_space("/thisshouldnotexist/");
    assert_true(diskspace == -2);

    // path missing in arg
    diskspace = get_free_disk_space(NULL);
    assert_true(diskspace == -1);

    // path missing in arg
    diskspace = get_free_disk_space("");
    assert_true(diskspace == -1);
}

void rlyeh_test_mkdir(UNUSED void** state) {
    int ret;
    bool ret2;
    char* temporary_dir;
    char* temporary_dir2;

    // should succeed --> non recursive
    temporary_dir = tempnam("/tmp/", "rlyeh");
    ret = rlyeh_mkdir(temporary_dir, false);
    assert_int_equal(ret, 0);
    ret2 = dir_exists(temporary_dir);
    assert_true(ret2);
    // should still succeed even if it is there
    ret = rlyeh_mkdir(temporary_dir, false);
    assert_int_equal(ret, 0);
    free(temporary_dir);

    // nested directories --> should fail, non recursive
    temporary_dir = "/tmp/madeuprlyeh/";
    temporary_dir2 = "/tmp/madeuprlyeh/madeupagain/";
    ret = rlyeh_mkdir(temporary_dir2, false);
    assert_int_not_equal(ret, 0);
    ret2 = dir_exists(temporary_dir);
    assert_false(ret2);
    ret2 = dir_exists(temporary_dir2);
    assert_false(ret2);

    // nested directories --> should succeed, recursive
    ret = rlyeh_mkdir(temporary_dir2, true);
    assert_int_equal(ret, 0);
    ret2 = dir_exists(temporary_dir);
    assert_true(ret2);
    ret2 = dir_exists(temporary_dir2);
    assert_true(ret2);

    // should fail
    ret = rlyeh_mkdir(NULL, true);
    assert_int_not_equal(ret, 0);

    ret = rlyeh_mkdir("", true);
    assert_int_not_equal(ret, 0);
}

#define STRING_TO_WRITE_IN_FILE "rlyeh\n"

void rlyeh_test_create_file(UNUSED void** state) {
    int ret;
    bool ret2;
    char* temporary_file;

    temporary_file = tempnam("/tmp/", "rlyeh");
    ret2 = file_exists(temporary_file);
    assert_false(ret2);
    ret = rlyeh_create_file(temporary_file);
    assert_int_equal(ret, 0);
    ret2 = file_exists(temporary_file);
    assert_true(ret2);
    free(temporary_file);

    temporary_file = "/tmp/madeuprlyeh2/madeupagain/";
    ret2 = file_exists(temporary_file);
    assert_false(ret2);
    ret = rlyeh_create_file(temporary_file);
    assert_true(ret < 0);
    ret2 = file_exists(temporary_file);
    assert_false(ret2);

    ret = rlyeh_create_file("");
    assert_true(ret < 0);

    ret = rlyeh_create_file(NULL);
    assert_true(ret < 0);
}

#define DIR_BASE "/tmp/rlyeh/copybasedir/"
#define DIR_NESTED1 "nested1/"
#define DIR_NESTED2 DIR_NESTED1 "nested2/"
#define DIR_NESTED3 DIR_NESTED2 "nested3/"
#define FILE_SOME DIR_NESTED1 "somefile"
#define DIR_BASE_DEST "/tmp/rlyehcopydest/"

void rlyeh_test_copy_dir_recursively(UNUSED void** state) {
    int ret;
    bool ret2;
    char* temporary_dir;

    // setup - used in symlink test as well
    ret = rlyeh_mkdir(DIR_BASE DIR_NESTED3, true);
    assert_int_equal(ret, 0);
    ret2 = dir_exists(DIR_BASE DIR_NESTED3);
    assert_true(ret2);

    ret = rlyeh_write_file(DIR_BASE FILE_SOME, "something");
    assert_int_equal(ret, strlen("something"));
    ret2 = file_exists(DIR_BASE FILE_SOME);
    assert_true(ret2);

    // destination dir not there yet
    ret = rlyeh_copy_dir_recursively(DIR_BASE, DIR_BASE_DEST);
    assert_true(ret < 0);

    ret = rlyeh_mkdir(DIR_BASE_DEST, false);
    assert_int_equal(ret, 0);
    ret2 = dir_exists(DIR_BASE_DEST);
    assert_true(ret2);

    ret = rlyeh_copy_dir_recursively(DIR_BASE, DIR_BASE_DEST);
//    assert_int_equal(ret, 0);

    ret2 = dir_exists(DIR_BASE_DEST DIR_NESTED3);
    assert_true(ret2);

    ret2 = file_exists(DIR_BASE_DEST FILE_SOME);
    assert_true(ret2);

    temporary_dir = tempnam("/tmp/", "rlyeh");
    ret = rlyeh_copy_dir_recursively(temporary_dir, DIR_BASE_DEST "/shouldfail");
    assert_true(ret < 0);
    free(temporary_dir);

    temporary_dir = "/tmp/rlyehmadeup3/rlyehmadeup";
    ret = rlyeh_copy_dir_recursively(DIR_BASE, temporary_dir);
    assert_true(ret < 0);

    ret = rlyeh_copy_dir_recursively("", DIR_BASE_DEST);
    assert_true(ret < 0);
    ret = rlyeh_copy_dir_recursively(NULL, DIR_BASE_DEST);
    assert_true(ret < 0);

    ret = rlyeh_copy_dir_recursively(DIR_BASE, "");
    assert_true(ret < 0);
    ret = rlyeh_copy_dir_recursively(DIR_BASE, NULL);
    assert_true(ret < 0);
}

#define DIR_BASE_DEST_SYMLINK "/tmp/rlyehsymlinktest/"

void rlyeh_test_symlink_dir_recursively(UNUSED void** state) {
    int ret;
    bool ret2;
    char* temporary_dir;
    struct stat buf;

    ret = rlyeh_symlink_files_in_dir_recursively(DIR_BASE, DIR_BASE_DEST_SYMLINK);
    assert_true(ret < 0);

    ret = rlyeh_mkdir(DIR_BASE_DEST_SYMLINK, false);
    assert_int_equal(ret, 0);
    ret2 = dir_exists(DIR_BASE_DEST_SYMLINK);
    assert_true(ret2);

    ret = rlyeh_symlink_files_in_dir_recursively(DIR_BASE, DIR_BASE_DEST_SYMLINK);
//    assert_int_equal(ret, 0);

    ret2 = dir_exists(DIR_BASE_DEST_SYMLINK DIR_NESTED3);
    assert_true(ret2);

    ret2 = file_exists(DIR_BASE_DEST_SYMLINK FILE_SOME);
    assert_true(ret2);

    ret = stat(DIR_BASE_DEST_SYMLINK FILE_SOME, &buf);
    assert_int_equal(ret, 0);
    assert_true(S_ISREG(buf.st_mode));

    ret = lstat(DIR_BASE_DEST_SYMLINK FILE_SOME, &buf);
    assert_int_equal(ret, 0);
    assert_true(S_ISLNK(buf.st_mode));

    temporary_dir = tempnam("/tmp/", "rlyeh");
    ret = rlyeh_symlink_files_in_dir_recursively(temporary_dir, DIR_BASE_DEST "/shouldfail");
    assert_true(ret < 0);
    free(temporary_dir);

    temporary_dir = "/tmp/rlyehmadeup4/rlyehmadeup";
    ret = rlyeh_symlink_files_in_dir_recursively(DIR_BASE, temporary_dir);
    assert_true(ret < 0);

    ret = rlyeh_symlink_files_in_dir_recursively("", DIR_BASE_DEST);
    assert_true(ret < 0);
    ret = rlyeh_symlink_files_in_dir_recursively(NULL, DIR_BASE_DEST);
    assert_true(ret < 0);

    ret = rlyeh_symlink_files_in_dir_recursively(DIR_BASE, "");
    assert_true(ret < 0);
    ret = rlyeh_symlink_files_in_dir_recursively(DIR_BASE, NULL);
    assert_true(ret < 0);
}
