include ../makefile.inc

# build destination directories
OBJDIR = ../output/$(MACHINE)

# TARGETS
TARGET = $(OBJDIR)/$(COMPONENT).so


# directories
# source directories
SRCDIR = $(shell pwd)
INCDIR_PRIV = $(SRCDIR)/../include_priv
INCDIRS = $(INCDIR_PRIV) $(if $(STAGINGDIR), $(STAGINGDIR)/include $(STAGINGDIR)/usr/include)
STAGINGLIBDIR = $(if $(STAGINGDIR), -L$(STAGINGDIR)/lib -L$(STAGINGDIR)/usr/lib)

# files
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))

CFLAGS +=  -fPIC
# compilation and linking flags
CFLAGS_RLYEH = $(CFLAGS) -Werror -Wall -Wextra \
          -Wformat=2 -Wshadow -Wwrite-strings \
          -Wredundant-decls \
          -Wmissing-declarations -Wno-attributes \
          -Wno-format-nonliteral \
          -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
          -fPIC -g3 $(addprefix -I ,$(INCDIRS))

ifeq ($(CC_NAME),g++)
    CFLAGS_RLYEH += -std=c++2a
else
    CFLAGS_RLYEH += -Wstrict-prototypes -Wold-style-definition -Wnested-externs -std=c11
endif

LDFLAGS += -fPIC

LDFLAGS_RLYEH = $(LDFLAGS) $(STAGINGLIBDIR) \
           -shared \
           -lamxc -lamxp -lamxd \
           -lamxo -lrlyeh \
           -lsahtrace -lpthread -llcm

#configuration for libocispec
AUTO_HOST=$(shell $(CC) -dumpmachine)
CONF="--host=$(AUTO_HOST)"

# targets
all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) -Wl,-soname,$(TARGET) -o $@ $(OBJECTS) $(LDFLAGS_RLYEH)

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS_RLYEH) -c -o $@ $<
	@$(CC) $(CFLAGS_RLYEH) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	$(MKDIR) -p $@

clean:
	rm -rf ../output/

.PHONY: all clean
