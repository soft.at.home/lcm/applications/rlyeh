/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>

#include <debug/sahtrace.h>
#include <yajl/yajl_gen.h>
#include <yajl/yajl_parse.h>
#include <amxc/amxc_string.h>
#include <amxc/amxc_variant_type.h>
#include <lcm/lcm_dump_rpc.h>

#include <rlyeh/rlyeh.h>

#include "rlyeh.h"
#include "rlyeh_dm.h"
#include "rlyeh_dm_notif.h"
#include "rlyeh_worker.h"
#include "rlyeh_worker_func.h"
#include "rlyeh_common.h"
#include "rlyeh_worker_signals.h"

#include <lcm/lcm_assert.h>

#define ME "rlyeh_functions"

static amxd_status_t create_image_dm_from_manifest(const char* disk_location, rlyeh_imagespec_manifest_element_t* manifest) {
    amxd_status_t status = amxd_status_invalid_arg;
    amxd_object_t* images = NULL;
    amxd_object_t* image = NULL;
    amxd_dm_t* dm = rlyeh_get_dm();
    amxd_trans_t trans;

    ASSERT_STR_NOT_EMPTY(manifest->duid, goto exit, "Can't create DM entry : No DUID provided");
    ASSERT_STR_NOT_EMPTY(manifest->duid, goto exit, "Can't create DM entry : No version provided");

    image = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].",
                          manifest->duid, manifest->version);

    amxd_trans_init(&trans);

    if(image) {
        amxd_trans_select_object(&trans, image);
    } else {
        images = amxd_dm_findf(dm, RLYEH_DM_IMAGES);
        amxd_trans_select_object(&trans, images);
        amxd_trans_add_inst(&trans, 0, NULL);

        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_DUID, manifest->duid);
        amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_VERSION, manifest->version);
    }

    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_URI, manifest->uri ? manifest->uri : "");
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_NAME, manifest->name ? manifest->name : "");
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_DISKLOCATION, disk_location ? disk_location : "");
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_VENDOR, manifest->vendor ? manifest->vendor : "");
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_DESCRIPTION, manifest->description ? manifest->description : "");

    amxd_trans_set_value(bool, &trans, RLYEH_DM_IMAGE_MARK_RM, manifest->mark_for_removal);
    amxd_trans_set_value(cstring_t, &trans, RLYEH_DM_IMAGE_STATUS, RLYEH_STATUS_DOWNLOADED);
    amxd_trans_set_value(uint32_t, &trans, RLYEH_DM_IMAGE_ERROR_CODE, 0);

    status = amxd_trans_apply(&trans, dm);

    amxd_trans_clean(&trans);
exit:
    return status;
}

typedef struct sync_images_args {
    amxd_status_t status;
    unsigned int failed_counter;
} sync_images_args_t;

static void rlyeh_sync_image(rlyeh_imagespec_index_t* index, rlyeh_imagespec_manifest_element_t* manifest, void* args) {
    sync_images_args_t* parsed_args = (sync_images_args_t*) args;
    amxd_status_t status = create_image_dm_from_manifest(index->image_disk_location, manifest);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Couldnt create %s:%s: (%d) %s", manifest->duid, manifest->version, status, amxd_status_string(status));
        parsed_args->status = status;
        parsed_args->failed_counter++;
    }
}

static void rlyeh_sync_images_dm(const char* image_location) {
    rlyeh_images_list_t images_list;
    sync_images_args_t args;
    args.status = amxd_status_ok;
    args.failed_counter = 0;

    rlyeh_images_list_init(&images_list);
    if(rlyeh_images_list_build(&images_list, image_location) > 0) {
        rlyeh_loop_over_images_list(&images_list, rlyeh_sync_image, &args);
        if(args.status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed syncing #%u images to dm (%d)", args.failed_counter, args.status);
        }
    }

    rlyeh_images_list_clean(&images_list);
}

amxd_status_t _Rlyeh_pull(amxd_object_t* obj,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, "password");
    amxd_status_t status = amxd_status_ok;

    int res = rlyeh_worker_add_task(obj, args, rlyeh_exec_pull, true, false);
    if(res != 0) {
        status = amxd_status_unknown_error;
        SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    return status;
}

amxd_status_t _Rlyeh_remove(UNUSED amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_ok;
    SAH_TRACEZ_INFO(ME, "Executing Remove. DUID [%s] Version [%s] CID [%s]",
                    GET_CHAR(args, RLYEH_CMD_PULL_DUID),
                    GET_CHAR(args, RLYEH_DM_IMAGE_VERSION),
                    GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID));

    int res = rlyeh_worker_add_task(obj, args, rlyeh_exec_remove, true, false);
    if(res != 0) {
        status = amxd_status_unknown_error;
        SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    return status;
}

amxd_status_t _Rlyeh_gc(amxd_object_t* obj,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_ok;

    int res = rlyeh_worker_add_task(obj, args, rlyeh_exec_gc, true, false);
    if(res != 0) {
        status = amxd_status_unknown_error;
        SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    return status;
}

amxd_status_t _Rlyeh_list(UNUSED amxd_object_t* obj,
                          UNUSED amxd_function_t* func,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);

    rlyeh_notif_image_list(command_id);

    status = amxd_status_ok;
    return status;
}

amxd_status_t _Rlyeh_status(UNUSED amxd_object_t* obj,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* image = NULL;
    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);
    const char* duid = GET_CHAR(args, RLYEH_DM_IMAGE_DUID);
    const char* version = GET_CHAR(args, RLYEH_DM_IMAGE_VERSION);

    amxd_dm_t* dm = rlyeh_get_dm();

    if(STRING_EMPTY(duid)) {
        status = amxd_status_invalid_arg;
        goto exit;
    }

    if(STRING_EMPTY(version)) {
        status = amxd_status_invalid_arg;
        goto exit;
    }

    image = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, version);
    if(image == NULL) {
        status = amxd_status_object_not_found;
        goto exit;
    }

    rlyeh_notif_image_status(image, command_id);
    status = amxd_status_ok;

exit:
    return status;
}

amxd_status_t _Rlyeh_sv(UNUSED amxd_object_t* obj,
                        UNUSED amxd_function_t* func,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    lcm_dump_rpc(obj, func, args, NULL);
    amxd_status_t status = amxd_status_invalid_arg;
    amxd_object_t* rlyeh = amxd_dm_get_object(rlyeh_get_dm(), RLYEH_DM);
    amxd_trans_t trans;
    bool sv_enable = GET_BOOL(args, RLYEH_CMD_SV_ENABLE);
    amxc_var_t* enable_var = GET_ARG(args, RLYEH_CMD_SV_ENABLE);

    if(enable_var) {
        amxc_var_delete(&enable_var);
    } else {
        goto exit;
    }

    if(sv_enable) {
        SAH_TRACEZ_NOTICE(ME, "Enabling SV (signature verification)");
    } else {
        SAH_TRACEZ_NOTICE(ME, "Disabling SV (signature verification)");
    }

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, rlyeh);
    amxd_trans_set_value(bool, &trans, RLYEH_SIGNATURE_VERIFICATION, sv_enable);
    status = amxd_trans_apply(&trans, rlyeh_get_dm());
    amxd_trans_clean(&trans);
exit:
    return status;
}

/**
 * Call a generic command. Called from the DM
 *
 * @return amxd_status_t amxd_status_ok on success
 */
amxd_status_t _Rlyeh_command(UNUSED amxd_object_t* obj,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;

    const char* command = GET_CHAR(args, RLYEH_COMMAND);
    const char* command_id = GET_CHAR(args, RLYEH_NOTIF_COMMAND_ID);
    amxc_var_t* params = GET_ARG(args, RLYEH_COMMAND_PARAMETERS);
    amxc_var_t* params_new = NULL;

    if(!STRING_EMPTY(command_id)) {
        if(params == NULL) {
            amxc_var_new(&params_new);
            amxc_var_set_type(params_new, AMXC_VAR_ID_HTABLE);
            params = params_new;
        }
        amxc_var_add_key(cstring_t, params, RLYEH_NOTIF_COMMAND_ID, command_id);
    }

    status = amxd_object_invoke_function(obj, command, params, ret);

    if(params_new) {
        amxc_var_delete(&params_new);
    }
    return status;
}

void rlyeh_pull_cb(amxd_object_t* obj, amxc_var_t* data) {
    amxd_status_t status = amxd_status_ok;
    tr181_fault_type_t cmd_status = tr181_fault_ok;
    amxd_dm_t* dm = rlyeh_get_dm();
    amxd_object_t* rlyeh = NULL;
    char* storage_location = NULL;
    char* image_location = NULL;
    const char* uri = NULL;
    const char* duid = NULL;
    const char* command_id = NULL;
    const char* disk_location = NULL;
    const char* version = NULL;
    rlyeh_image_parameters_t uri_elements;
    rlyeh_imagespec_manifest_element_t manifest;
    amxd_object_t* image = NULL;

    rlyeh_imagespec_manifest_element_init(&manifest);
    when_null(obj, exit);
    when_null(data, exit);

    rlyeh_image_parameters_init(&uri_elements);

    rlyeh = amxd_dm_get_object(dm, RLYEH_DM);
    when_null(rlyeh, exit);
    storage_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_STORAGE_LOCATION, NULL);
    image_location = amxd_object_get_value(cstring_t, rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    when_null(storage_location, exit);
    when_null(image_location, exit);
    uri = GET_CHAR(data, RLYEH_CMD_PULL_URI);
    duid = GET_CHAR(data, RLYEH_CMD_PULL_DUID);
    version = GET_CHAR(data, RLYEH_CMD_PULL_VERSION);
    command_id = GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID);
    disk_location = GET_CHAR(data, RLYEH_CMD_PULL_IMAGE_DISKLOCATION);
    cmd_status = (tr181_fault_type_t) GET_INT32(data, RLYEH_CMD_RES);
    if(cmd_status != tr181_fault_ok) {
        SAH_TRACEZ_ERROR(ME, "Pull failed for %s [%s] with error [%s]", duid, uri, tr181_fault_type_to_string(cmd_status));
        goto exit;
    }

    if(rlyeh_imagespec_manifest_element_get_from_annotations(disk_location, storage_location, duid, version, &manifest)) {
        SAH_TRACEZ_ERROR(ME, "Couldnt parse annotations of [%s:%s] in %s", duid, version, disk_location);
    }

    if(manifest.uri == NULL) {
        amxc_string_t complete_uri;
        amxc_string_init(&complete_uri, 0);
        rlyeh_parse_uri(uri, &uri_elements);
        if((uri_elements.server == NULL) || (uri_elements.server[0] == '\0')) {
            rlyeh_get_server_from_file(&uri_elements);
        }
        rlyeh_generate_uri(&uri_elements, &complete_uri, NULL);
        manifest.uri = strdup(complete_uri.buffer);
        amxc_string_clean(&complete_uri);
    } else {
        rlyeh_parse_uri(manifest.uri, &uri_elements);
    }

    if(manifest.name == NULL) {
        manifest.name = strdup(uri_elements.image_name);
    }
    if(manifest.vendor == NULL) {
        manifest.vendor = strdup("");
    }
    if(manifest.description == NULL) {
        manifest.description = strdup("");
    }
    if(manifest.version == NULL) {
        manifest.version = strdup(version);
    }

    status = create_image_dm_from_manifest(disk_location, &manifest);

    switch(status) {
    case amxd_status_ok:
        image = amxd_dm_findf(dm, RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, version);
        if(image) {
            rlyeh_notif_image_pulled(image, command_id);
        } else {
            SAH_TRACEZ_ERROR(ME, "Can't find new image [%s:%s]", duid, manifest.version);
        }
        break;
    case amxd_status_invalid_arg:
        NOTIF_FAILED(obj, command_id, tr181_fault_invalid_arguments, RLYEH_CMD_PULL, "%s", tr181_fault_type_to_string(tr181_fault_invalid_arguments));
        break;
    default:
        NOTIF_FAILED(obj, command_id, tr181_fault_request_denied, RLYEH_CMD_PULL, "unknown error [%d]", status);
        break;
    }

exit:
    rlyeh_worker_signal_task_complete();
    rlyeh_imagespec_manifest_element_clean(&manifest);
    rlyeh_image_parameters_clean(&uri_elements);
    free(storage_location);
    free(image_location);
}

static void update_mark_for_removal(amxd_object_t* instance) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, RLYEH_DM_IMAGE_MARK_RM, true);

    amxd_trans_apply(&trans, rlyeh_get_dm());
    amxd_trans_clean(&trans);
}

void rlyeh_remove_cb(amxd_object_t* obj, amxc_var_t* data) {
    if(!obj) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    const char* command_id = GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID);
    const char* duid = GET_CHAR(data, RLYEH_CMD_PULL_DUID);
    const char* version = GET_CHAR(data, RLYEH_DM_IMAGE_VERSION);
    amxd_object_t* image = amxd_dm_findf(rlyeh_get_dm(), RLYEH_DM_IMAGES ".[" RLYEH_DM_IMAGE_DUID " == '%s' && " RLYEH_DM_IMAGE_VERSION " == '%s'].", duid, version);
    if(image == NULL) {
        NOTIF_FAILED(obj,
                     command_id,
                     tr181_fault_request_denied,
                     RLYEH_CMD_REMOVE,
                     "Could't find image with " RLYEH_DM_IMAGE_DUID " (%s) - " RLYEH_DM_IMAGE_VERSION " (%s)",
                     duid,
                     version);
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Mark Image for remove in DM");
    update_mark_for_removal(image);

    rlyeh_notif_image_remove_mark(image, command_id);
exit:
    return;
}

void rlyeh_image_removed_cb(amxd_object_t* obj, amxc_var_t* data) {
    if(!obj) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    bool delete = false;
    amxd_object_for_each(instance, it, obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool remove = amxd_object_get_value(bool, instance, RLYEH_DM_IMAGE_MARK_RM, NULL);
        if(remove) {
            rlyeh_notif_image_removed(instance, GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID));
            // remove instance from dm
            amxd_object_delete(&instance);
            delete = true;
        }
    }

    // Call a second time gc(), in some cases there might be some orphan image spec to remove
    if(delete) {
        amxc_var_t new_data;
        amxc_var_init(&new_data);
        if(data != NULL) {
            amxc_var_copy(&new_data, data);
        }
        int res = rlyeh_worker_add_task(obj, &new_data, rlyeh_exec_gc, true, true);
        if(res != 0) {
            SAH_TRACEZ_ERROR(ME, "Couldn't add task to worker's queue");
        }
        amxc_var_clean(&new_data);
    }
exit:
    rlyeh_worker_signal_task_complete();
    return;
}

void rlyeh_cmd_failed(amxd_object_t* obj, amxc_var_t* data) {
    if(!obj) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    if(!data) {
        SAH_TRACEZ_WARNING(ME, "No data");
        goto exit;
    }
    NOTIF_FAILED(obj, GET_CHAR(data, RLYEH_NOTIF_COMMAND_ID),
                 GET_INT32(data, RLYEH_NOTIF_ERROR_TYPE),
                 GET_CHAR(data, RLYEH_COMMAND),
                 "%s", GET_CHAR(data, RLYEH_NOTIF_ERROR_REASON));
exit:
    return;
}

static int rlyeh_execute_onboarding(amxd_object_t* rlyeh, const char* image_location, const char* storage_location) {
    int ret = 0;
    char* ro_image_location = amxd_object_get_cstring_t(rlyeh, RLYEH_IMAGE_LOCATION_RO, NULL);
    char* ro_storage_location = amxd_object_get_cstring_t(rlyeh, RLYEH_STORAGE_LOCATION_RO, NULL);

    if(!STRING_EMPTY(ro_image_location)) {
        SAH_TRACEZ_NOTICE(ME, "Copying image files from '%s' to '%s'", ro_image_location, image_location);
        if(dir_exists(ro_image_location)) {
            ret = rlyeh_copy_dir_recursively(ro_image_location, image_location);
            if(ret) {
                SAH_TRACEZ_ERROR(ME, "Error in copying image files (%d)", ret);
            }
        } else {
            SAH_TRACEZ_WARNING(ME, RLYEH_IMAGE_LOCATION_RO " directory does not exist '%s'", ro_image_location);
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "No " RLYEH_IMAGE_LOCATION_RO " defined");
    }

    if(!STRING_EMPTY(ro_storage_location)) {
        SAH_TRACEZ_NOTICE(ME, "Symlinking storage files from '%s' to '%s'", ro_storage_location, storage_location);
        if(dir_exists(ro_storage_location)) {
            ret = rlyeh_symlink_files_in_dir_recursively(ro_storage_location, storage_location);
            if(ret) {
                SAH_TRACEZ_ERROR(ME, "Error in symlinking storage files (%d)", ret);
            }
        } else {
            SAH_TRACEZ_WARNING(ME, RLYEH_STORAGE_LOCATION_RO " directory does not exist '%s'", ro_storage_location);
        }
    } else {
        SAH_TRACEZ_WARNING(ME, "No " RLYEH_STORAGE_LOCATION_RO " defined");
    }

    free(ro_storage_location);
    free(ro_image_location);
    return ret;
}

int rlyeh_init(amxd_dm_t* rlyeh_dm) {
    int rc = -1;
    amxd_object_t* rlyeh = NULL;
    char* image_location = NULL;
    char* storage_location = NULL;
    char* onboarding_file = NULL;

    rlyeh = amxd_dm_get_object(rlyeh_dm, RLYEH_DM);
    image_location = amxd_object_get_cstring_t(rlyeh, RLYEH_IMAGE_LOCATION, NULL);
    storage_location = amxd_object_get_cstring_t(rlyeh, RLYEH_STORAGE_LOCATION, NULL);
    onboarding_file = amxd_object_get_cstring_t(rlyeh, RLYEH_ONBOARDING_FILE, NULL);

    if(rlyeh_mkdir(image_location, true) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", image_location);
        goto exit;
    }
    if(rlyeh_mkdir(storage_location, true) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not make data dir [%s]", storage_location);
        goto exit;
    }

    if(!file_exists(onboarding_file)) {
        rlyeh_execute_onboarding(rlyeh, image_location, storage_location);
        if(rlyeh_create_file(onboarding_file)) {
            SAH_TRACEZ_ERROR(ME, "!!! Cannot create onboarded file !!! --> next boot we will onboard again, you probably dont want this");
        }
    }

    rlyeh_sync_images_dm(image_location);
    rc = 0;
exit:
    free(onboarding_file);
    free(image_location);
    free(storage_location);
    return rc;
}
